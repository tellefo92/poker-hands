# Poker hands
Picks 5 cards randomly from two decks of cards, and tells you if those 5 cards represent a poker hand. <br />
Displays the 5 cards with *beautiful* ASCII art.

## Requirements
Requires `cmake` and `cpp`

## Usage
Uses Cmake
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../
# Generate makefile for debug build
$ cmake ../ -DCMAKE_BUILD_TYPE=debug      
# Compile the project
$ make
#Run the compiled binary
$ ./main
```

## Maintainers
Tellef Østereng | [@tellefo92](https://gitlab.com/tellefo92)