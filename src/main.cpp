#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "time.h"

struct Card {
    int value;
    std::string suit;
};

std::vector<Card> generateDeck() {
    std::vector<Card> deck;
    std::vector<int> values = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    std::vector<std::string> suits = {"hearts", "spades", "clubs", "diamonds"};
    for (auto& value: values) {
        for (auto& suit: suits) {
            deck.push_back({value, suit});
            deck.push_back({value, suit});
        }
    }
    return deck;
}

void drawCards(std::vector<Card>& deck, std::vector<Card>& hand) {
    for (size_t i = 0; i < 5; ++i) {
        // Getting index of random card in remaining deck
        int num = std::rand() % deck.size();
        // Adding card with index num to hand
        hand.push_back(deck[num]);
        // Removing card with index num from deck
        deck.erase(deck.begin() + num);
    }
}

void sortHand(std::vector<Card>& hand) {
    std::vector<Card> sorted_hand;
    for (size_t i = 0; i < 5; ++i) {
        int lowest = hand[0].value;
        int lowest_index;
        for (size_t j = 0; j < hand.size(); ++j) {
            if (hand[j].value <= lowest) {
                lowest = hand[j].value;
                lowest_index = j;
            }
        }
        sorted_hand.push_back(hand[lowest_index]);
        hand.erase(hand.begin() + lowest_index);
    }
    hand = sorted_hand;
}

bool fiveSame(std::vector<Card>& hand) {
    Card prev_card = hand[0];
    for (size_t i = 1; i < 5; ++i) {
        if (hand[i].value == prev_card.value) {
            prev_card = hand[i];
        } else {
            return false;
        }
    }
    return true;
}

bool royalFlush(std::vector<Card>& hand) {
    Card prev_card = hand[0];
    if (prev_card.value != 10) {
        return false;
    }
    for (size_t i = 1; i < 5; ++i) {
        if (hand[i].value == prev_card.value + 1 && hand[i].suit == prev_card.suit) {
            prev_card = hand[i];
        } else {
            return false;
        }
    }
    return true;
}

bool straightFlush(std::vector<Card>& hand) {
    Card prev_card = hand[0];
    for (size_t i = 1; i < 5; ++i) {
        if (hand[i].value == prev_card.value + 1 && hand[i].suit == prev_card.suit) {
            prev_card = hand[i];
        } else {
            return false;
        }
    }
    return true;
}

bool fourSame(std::vector<Card>& hand) {
    std::map<int,int> count;
    for (size_t i = 0; i < 5; ++i) {
        count[hand[i].value]++;
    }
    if (count.size() > 2) {
        return false;
    } else {
        std::map<int,int>::iterator it;
        for (it = count.begin(); it != count.end(); ++it) {
            if (it->second == 4) {
                return true;
            }
        }
    }
    return false;
}

bool fullHouse(std::vector<Card>& hand) {
    std::map<int,int> count;
    // booleans to store if first key in map is pair or three of a kind
    bool two, three;
    for (size_t i = 0; i < 5; ++i) {
        count[hand[i].value]++;
    }
    if (count.size() > 2) {
        return false;
    } else {
        std::map<int,int>::iterator it;
        for (it = count.begin(); it != count.end(); ++it) {
            if (it->second == 3) {
                three = true;
            } else if (it->second == 2) {
                two = true;
            }
        }
        if (two && three) {
            return true;
        }
    }
    return false;
}

bool flush(std::vector<Card>& hand) {
    Card prev_card = hand[0];
    for (size_t i = 1; i < 5; ++i) {
        if (hand[i].suit == prev_card.suit) {
            prev_card = hand[i];
        } else {
            return false;
        }
    }
    return true;
}

bool straight(std::vector<Card>& hand) {
    Card prev_card = hand[0];
    for (size_t i = 1; i < 5; ++i) {
        if (hand[i].value == prev_card.value + 1) {
            prev_card = hand[i];
        } else {
            return false;
        }
    }
    return true;
}

bool threeSame(std::vector<Card>& hand) {
    std::map<int,int> count;
    for (size_t i = 0; i < 5; ++i) {
        count[hand[i].value]++;
    }
    if (count.size() > 3) {
        return false;
    } else {
        std::map<int,int>::iterator it;
        for (it = count.begin(); it != count.end(); ++it) {
            if (it->second == 3) {
                return true;
            }
        }
    }
    return false;
}

bool twoPair(std::vector<Card>& hand) {
    std::map<int,int> count;
    bool one_pair;
    for (size_t i = 0; i < 5; ++i) {
        count[hand[i].value]++;
    }
    if (count.size() > 3) {
        return false;
    } else {
        std::map<int,int>::iterator it;
        for (it = count.begin(); it != count.end(); ++it) {
            if (it->second == 2 && one_pair) {
                return true;
            } else if (it->second == 2 && !one_pair) {
                one_pair = true;
            }
        }
    }
    return false;
}

bool onePair(std::vector<Card>& hand) {
    std::map<int,int> count;
    for (size_t i = 0; i < 5; ++i) {
        count[hand[i].value]++;
    }
    if (count.size() > 4) {
        return false;
    } else {
        std::map<int,int>::iterator it;
        for (it = count.begin(); it != count.end(); ++it) {
            if (it->second == 2) {
                return true;
            }
        }
    }
    return false;
}

std::string asciiCard(Card card) {
    if (card.suit == "hearts") {
        return "♥";
    } else if (card.suit == "diamonds") {
        return "♦";
    } else if (card.suit == "clubs") {
        return "♣";
    } else if (card.suit == "spades") {
        return "♠";
    } else {
        return "0";
    }
}

void asciiHand(std::vector<Card>& hand) {
    for (size_t i = 0; i < 5; ++i) {
        std::cout << "*";
        for (size_t j = 0; j < 6; ++j) {
            std::cout << "-";
        }
        std::cout << "* ";
    }
    std::cout << std::endl;

    for (size_t i = 0; i < 5; ++i) {
        std::cout << "|";
        if (hand[i].value > 10) {
            switch (hand[i].value) {
                case 11:
                    std::cout << "J";
                    break;
                case 12:
                    std::cout << "Q";
                    break;
                case 13:
                    std::cout << "K";
                    break;
                case 14:
                    std::cout << "A";
                    break;
                default:
                    break;
            }
        } else {
            std::cout << hand[i].value;
        }
        int spaces;
        if (hand[i].value == 10) {
            spaces = 2;
        } else {
            spaces = 3;
        }
        for (size_t j = 0; j < spaces; ++j) {
            std::cout << " ";
        }
        std::cout << asciiCard(hand[i]) << " | ";
    }
    std::cout << std::endl;
    for (size_t i = 0; i < 2; ++i) {
        for (size_t j = 0; j < 5; ++j) {
            std::cout << "|      | ";
        }
        std::cout << std::endl;
    }
    for (size_t i = 0; i < 5; ++i) {
        std::cout << "|" << asciiCard(hand[i]);
        int spaces;
        if (hand[i].value == 10) {
            spaces = 3;
        } else {
            spaces = 4;
        }
        for (size_t j = 0; j < spaces; ++j) {
            std::cout << " ";
        }
        if (hand[i].value > 10) {
            switch (hand[i].value) {
                case 11:
                    std::cout << "J| ";
                    break;
                case 12:
                    std::cout << "Q| ";
                    break;
                case 13:
                    std::cout << "K| ";
                    break;
                case 14:
                    std::cout << "A| ";
                    break;
                default:
                    break;
            }
        } else {
            std::cout << hand[i].value << "| ";
        }
    }
    std::cout << std::endl;
    for (size_t i = 0; i < 5; ++i) {
        std::cout << "*";
        for (size_t j = 0; j < 6; ++j) {
            std::cout << "-";
        }
        std::cout << "* ";
    }
    std::cout << std::endl;
}

void checkHand(std::vector<Card>& hand) {
    if (fiveSame(hand)) {
        std::cout << "5 of a kind!" << std::endl;
    } else if (royalFlush(hand)) {
        std::cout << "Royal flush!" << std::endl;
    } else if (straightFlush(hand)) {
        std::cout << "Straight flush!" << std::endl;
    } else if (fourSame(hand)) {
        std::cout << "4 of a kind!" << std::endl;
    } else if (fullHouse(hand)) {
        std::cout << "Full house!" << std::endl;
    } else if (flush(hand)) {
        std::cout << "Flush!" << std::endl;
    } else if (straight(hand)) {
        std::cout << "Straight!" << std::endl;
    } else if (threeSame(hand)) {
        std::cout << "3 of a kind!" << std::endl;
    } else if (twoPair(hand)) {
        std::cout << "2 pairs!" << std::endl;
    } else if (onePair(hand)) {
        std::cout << "1 pair!" << std::endl;
    } else {
        std::cout << "High card: ";
        if (hand[4].value > 10) {
            switch (hand[4].value) {
                case 11:
                    std::cout << "Jack";
                    break;
                case 12:
                    std::cout << "Queen";
                    break;
                case 13:
                    std::cout << "King";
                    break;
                case 14:
                    std::cout << "Ace";
                    break;
                default:
                    break;
            }
        } else {
            std::cout << hand[4].value;
        }
        std::cout << " of " << hand[4].suit << std::endl;
    }
    asciiHand(hand);
}

int main() {
    // Set random seed from time
    std::srand(time(NULL));
    // Generate two decks of 52 cards
    std::vector<Card> deck = generateDeck();
    std::vector<Card> hand;
    std::cout << "Drawing 5 random cards.." << std::endl;
    drawCards(deck, hand);
    sortHand(hand);
    checkHand(hand);
    return 0;
}